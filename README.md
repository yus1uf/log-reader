# log-reader

## Getting started

To make it easy for you here's a list of recommended next steps.
Clone the project from gitlab, Follow below steps

```
git clone https://gitlab.com/yus1uf/log-reader.git
cd log-reader
```

## Add your logs files

- [ ] Change the path in constant file to the your log file

## Run the Application

Follow below command to run the application

```
npm run start
```

## API Structure

- [ ] [http://localhost:3000/timestamp/?start=<your_timestamp>&end=<your_timestamp>](http://localhost:3000/timestamp/?start=2020-01-16T00:54:46.695Z&end=2020-01-18T07:34:09.451Z)
