import fs from "fs";
/**
 * It takes position and logFilePath as parameter and return timestamp at given position
 * @param {Number} position
 * @param {String} logFilePath
 * @returns { String }  line from log file
 */
export const readTimestampFromPosition = (position, logFilePath) => {
  const buffer = Buffer.alloc(256);
  const fd = fs.openSync(logFilePath, "r");
  fs.readSync(fd, buffer, 0, buffer.length, position);
  fs.closeSync(fd);
  const line = buffer.toString("utf-8").split("\n")[1];
  return line.split(" ")[0];
};
