import { readTimestampFromPosition } from "./readFromPosition.js";
/**
 * It takes unix time, log file and log file size size as parameter and calculate specific position
 * @param {Number} targetTimestamp
 * @param {Number} logFileSize
 * @param {String} logFilePath
 * @returns {Number} [targetd timestamp file position]
 */
export const findPositionForTimestamp = (
  targetTimestamp,
  logFileSize,
  logFilePath
) => {
  let low = 0;
  let high = logFileSize - 1;
  let mid = Math.floor((low + high) / 2); //calculate mid position of file

  while (low <= high) {
    mid = Math.floor((low + high) / 2);
    const midTimestamp = readTimestampFromPosition(mid, logFilePath); //read log from mid position and fetch timestamp
    let unixTimestamp = Math.floor(new Date(midTimestamp).getTime() / 1000); //calculate mid timestamp into unix format

    //check mid timestamp is equal to targeted timestamp
    if (unixTimestamp === targetTimestamp) {
      return mid;
    }
    //check if mid timestamp is lesser than targeted timestamp
    if (unixTimestamp < targetTimestamp) {
      low = mid + 1;
    } else {
      high = mid - 1;
    }
  }
};
