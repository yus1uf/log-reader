export const APP = {
  PORT: 3000,
};


export const StatusCode = {
  ERROR: 500,
  NOT_FOUND: 404,
  SUCCESS: 200,
  INVALID: 400,
};

export const Message = {
  ERROR_MESSAGE: "Error reading log file.",
  NOT_FOUND_MESSAGE: "Timestamp not found in the log file.",
  INVALID_MESSAGE: "Invalid timestamp parameter.",
};

export const Path = {
  FILE_PATH: 'src/example/example (3).txt'
}
