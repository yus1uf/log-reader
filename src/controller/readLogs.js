import fs from "fs";
import { findPositionForTimestamp } from "../helper/findPosition.js";
import { StatusCode, Message, Path } from "../constant/constant.js";

/**
 * It takes start and end timestamp and response object and return specific data
 * @param {Number} startTimestamp
 * @param {Number} endTimestamp
 * @param {Object} res
 * @returns Response to the client with Data
 */
export const readLog = (startTimestamp, endTimestamp, res) => {
  try {
    const logFilePath = Path.FILE_PATH;
    const logFileSize = fs.statSync(logFilePath).size;
    //calculate start posintion
    const startPosition = findPositionForTimestamp(
      startTimestamp,
      logFileSize,
      logFilePath
    );

    //calculate end position
    const endPosition = findPositionForTimestamp(
      endTimestamp,
      logFileSize,
      logFilePath
    );

    if (startPosition === -1 || endPosition === -1) {
      res.statusCode = StatusCode.NOT_FOUND;
      res.end(Message.NOT_FOUND_MESSAGE);
      return;
    }

    const chunkSize = endPosition - startPosition + 100; //calculate buffer size

    const buffer = Buffer.alloc(chunkSize); //allocate buffer
    const fd = fs.openSync(logFilePath, "r");

    fs.read(
      fd,
      buffer,
      0,
      chunkSize,
      startPosition,
      (err, bytesRead, chunk) => {
        if (err) {
          res.statusCode = StatusCode.ERROR;
          res.end(Message.ERROR_MESSAGE);
        } else {
          res.setHeader("Content-Type", "text/plain");
          res.end(chunk.toString("utf-8", 0, bytesRead));
        }
        fs.closeSync(fd);
      }
    );
  } catch (error) {
    console.log(error);
    res.statusCode = StatusCode.ERROR;
    res.end(Message.ERROR_MESSAGE);
  }
};
