import http from "http";
import { APP, Message, StatusCode } from "./src/constant/constant.js";
import { readLog } from "./src/controller/readLogs.js";

const server = http.createServer((req, res) => {
  if (req.url != "/favicon.ico") {
    const urlParts = new URL(req.url, `http://${req.headers.host}`);

    const startTimestamp = urlParts.searchParams.get("start"); //extract start timestamp from request uri
    const endTimestamp = urlParts.searchParams.get("end"); //extract start timestamp from request uri
    // convert timestamp into unix format
    let startTimestampInUnix = Math.floor(
      new Date(startTimestamp).getTime() / 1000
    );
    let endTimestampInUnix = Math.floor(
      new Date(endTimestamp).getTime() / 1000
    );

    if (isNaN(startTimestampInUnix) || isNaN(endTimestampInUnix)) {
      res.statusCode = StatusCode.INVALID;
      res.end(Message.INVALID_MESSAGE);
      return;
    }
    readLog(startTimestampInUnix, endTimestampInUnix, res);
  }
});

const port = APP.PORT || 3000;
server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
